# Importing flask module in the project is mandatory
# An object of Flask class is our WSGI application.
from flask import Flask, render_template, request
import os

IMG_FOLDER = os.path.join('static', 'IMG')

# Flask constructor takes the name of
# current module (__name__) as argument.
app = Flask(
    __name__,
    template_folder='templates',
    static_folder='static'
    )

app.config['UPLOAD_FOLDER'] = IMG_FOLDER

# The route() function of the Flask class is a decorator,
# which tells the application which URL should call
# the associated function.
main_icon = os.path.join(app.config['UPLOAD_FOLDER'], 'Smiley.png')
one_icon = os.path.join(app.config['UPLOAD_FOLDER'], 'Smiley4.png')
two_icon = os.path.join(app.config['UPLOAD_FOLDER'], 'Smiley3.png')
about_icon = os.path.join(app.config['UPLOAD_FOLDER'], 'PythonLounge.png')


@app.route("/", methods=['GET', 'POST'])
@app.route("/home", methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if request.form.get('action1') == 'Chapter 1':
            return render_template('chap1.html', user_image=one_icon)
        elif request.form.get('action2') == 'Chapter 2':
            return render_template('chap2.html', user_image=two_icon)
        elif request.form.get('action3') == 'About':
            return render_template('about.html', user_image=about_icon)
        else:
            pass  # unknown
    elif request.method == 'GET':
        return render_template('index.html', user_image=main_icon)
    return render_template("index.html", user_image=main_icon)


@app.route('/Chap1', methods=['GET', 'POST'])
def show_chap1():
    if request.method == 'POST':
        if request.form.get('action2') == 'Chapter 2':
            return render_template('chap2.html', user_image=two_icon)
        elif request.form.get('action4') == 'Main':
            return render_template('index.html', user_image=main_icon)
        elif request.form.get('action3') == 'About':
            return render_template('about.html', user_image=about_icon)
        else:
            pass  # unknown
    elif request.method == 'GET':
        return render_template("chap1.html", user_image=one_icon)
    return render_template("chap1.html", user_image=one_icon)


@app.route('/Chap2')
def show_chap2():
    if request.method == 'POST':
        if request.form.get('action1') == 'Chapter 1':
            return render_template('chap1.html', user_image=one_icon)
        elif request.form.get('action4') == 'Main':
            return render_template('index.html', user_image=main_icon)
        elif request.form.get('action3') == 'About':
            return render_template('about.html', user_image=about_icon)
        else:
            pass  # unknown
    elif request.method == 'GET':
        return render_template("chap2.html", user_image=two_icon)
    return render_template('chap2.html', user_image=two_icon)


@app.route('/About')
def about():
    # about_icon = os.path.join(app.config['UPLOAD_FOLDER'], 'PythonLounge.png')
    return render_template('about.html', user_image=about_icon)
    

# main driver function
if __name__ == '__main__':
    app.run(debug=True,
            host='0.0.0.0',  # open in all options for pages
            port=5000  # use port 5000
            )
